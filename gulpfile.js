/**
 * @file
 * Gulp file to automate tasks.
 */

(function () {

  'use strict';

  var gulp = require('gulp'),
      sass = require('gulp-sass'),
      typescript = require('gulp-typescript'),
      uglify = require('gulp-uglify'),
      sourcemaps = require('gulp-sourcemaps'),
      notify = require('gulp-notify'),
      imagemin = require('gulp-imagemin'),
      browserSync = require('browser-sync');

  var reload = browserSync.reload;
  var config = require('./gulpfile-config.json');

  // Error notifications.
  var reportError = function(error) {
    notify({
      title: 'Gulp Task Error',
      message: 'Check the console.'
    }).write(error);
    console.log(error.toString());
    this.emit('end');
  };

  // Sass processing.
  function processSass(directory) {
    return gulp.src([
      directory + '/**/*.scss'
      ])
      .pipe(sourcemaps.init())
      // Convert sass into css.
      .pipe(sass({
        outputStyle: config.sass.outputStyle,
      }))
      // Show errors.
        .on('error', reportError)
      // Write sourcemaps.
        .pipe(sourcemaps.write('./../sourcemaps/' + directory))
      // Save css.
        .pipe(gulp.dest(directory))
        .pipe(browserSync.reload({
          stream: true
        }));
  }

  gulp.task('styles-sass', function() {
    return processSass('styles');
  });
  gulp.task('components-sass', function() {
    return processSass('components');
  });
  gulp.task('settings-sass', function() {
    return processSass('settings');
  });

  // TypeScript processing.
  function processTs(directory) {
    return gulp.src([
      directory + '/**/*.ts'
      ])
      .pipe(sourcemaps.init())
      .pipe(typescript({
        'target': 'ES5',
        'module': 'commonjs',
        'removeComments': true,
        'noImplicitAny': false
      }))
      // Show errors.
        .on('error', reportError)
      // Minify script.
        .pipe(uglify())
      // Write sourcemaps.
        .pipe(sourcemaps.write('./../sourcemaps/' + directory))
        .pipe(gulp.dest(directory))
        .pipe(browserSync.reload({
          stream: true
        }));
  }

  gulp.task('scripts-ts', function () {
    return processTs('scripts');
  });
  gulp.task('components-ts', function () {
    return processTs('components');
  });

  // Optimize Images.
  gulp.task('images', function() {
    return gulp.src('images/**/*')
      .pipe(imagemin({
        progressive: true,
        interlaced: true,
        svgoPlugins: [{
          cleanupIDs: false
        }]
      }))
        .pipe(gulp.dest('images'));
  });

  // BrowserSync.
  gulp.task('browser-sync', function() {
    // Watch files.
    var files = [
      'styles/**/*.css',
      'components/**/*.css',
      'settings/**/*.css',
      'scripts/**/*.js',
      'components/**/*.js',
      'images/**/*',
    ];
    // Initialize browsersync.
    browserSync.init(files, {
      proxy: config.browserSync.proxy
    });
  });

  // Default task to be run with `gulp`.
  gulp.task('default',
  ['styles-sass', 'components-sass', 'settings-sass',
  'scripts-ts', 'components-ts', 'browser-sync'],
  function() {
    gulp.watch([
      'styles/**/*.scss',
      ], ['styles-sass']);

    gulp.watch([
      'components/**/*.scss',
      ], ['components-sass']);

    gulp.watch([
      'settings/**/*.scss',
      ], ['settings-sass']);

    gulp.watch([
      'scripts/**/*.ts',
      ], ['scripts-ts']);

    gulp.watch([
      'components/**/*.ts',
      ], ['components-ts']);
  });
}());
